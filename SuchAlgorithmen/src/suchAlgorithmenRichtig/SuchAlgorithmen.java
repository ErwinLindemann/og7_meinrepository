package suchAlgorithmenRichtig;

import java.util.Scanner;
import java.util.Random;
import java.util.Arrays;

public class SuchAlgorithmen {

	public static void main(String[] args) {
		Scanner danny = new Scanner(System.in);
		Random rand = new Random();
		StoppUhr uhr1 = new StoppUhr();
		StoppUhr uhr2 = new StoppUhr();
		char x;
		do {
			System.out.print("Gr��e des Feldes: ");
			int anzahl = danny.nextInt();
			int[] feld = new int[anzahl];
			System.out.print("Gesuchte Zahl: ");
			int gesuchterWert = danny.nextInt();
			for (int i = 0; i < feld.length; i++) {
				feld[i] = rand.nextInt(20000000); // 2147483647
			}
			System.out.println();
			sortUhr(feld);
			System.out.println();
			System.out.println("Linearesuche:");
			uhr1.startM();
			linSuche(feld, gesuchterWert);
			uhr1.endM();
			System.out.println("Dauer: " + uhr1.getDauer() + " Millisekunden");
			System.out.println();
			System.out.println("Bin�resuche");
			uhr2.startM();
			binSuche(feld, gesuchterWert);
			uhr2.endM();
			System.out.println("Dauer: " + uhr2.getDauer() + " Millisekunden");
			System.out.println();
			System.out.println("Zum Wiederholen j");
			x = danny.next().charAt(0);
			System.out.println();
		} while (x == 'j');
		danny.close();
	}

	public static void linSuche(int[] feld, int gesuchterWert) {
		int index = -1;
		for (int i = 0; i < feld.length; i++) {
			if (feld[i] == gesuchterWert) {
				index = i;
			}
		}
		System.out.println("I:" + index);
	}

	public static void binSuche(int[] feld, int gesuchterWert) {
		int index = -1;
		int start = 0;
		int ende = feld.length - 1;
		while (start <= ende) {
			int mitte = (start + ende) / 2;
			if (gesuchterWert < feld[mitte]) {
				ende = mitte - 1;
			}
			if (gesuchterWert > feld[mitte]) {
				start = mitte + 1;
			}
			if (gesuchterWert == feld[mitte]) {
				index = mitte;
				start = ende + 1;
			}
		}
		System.out.println("I:" + index);
	}

	public static int[] sortUhr(int[] feld) {
		StoppUhr uhrSort = new StoppUhr();
		uhrSort.startM();
		Arrays.sort(feld);
		uhrSort.endM();
		System.out.println("Das Sortieren hat " + uhrSort.getDauer() + " Millisekunden gedauert");
		return feld;
	}

}
