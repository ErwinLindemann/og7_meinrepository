package fibonacci;
import java.util.Scanner;

public class FIBONACCI {


	public static void main(String[] args) {

		Scanner danny = new Scanner(System.in);
		String wiederholen;
		do {
			System.out.print("Anzahl Monate: ");
			int monat = danny.nextInt();
			System.out.println(fibo(monat));
			System.out.println("Widerholen ja = (j)");
			wiederholen = danny.next();
		}
		while(wiederholen.equals("j"));
		danny.close();
	}

	public static int fibo(int monat) {
		if (monat != 0) {
			if (monat == 1 || monat == 2) {
				return 1;
			}	
			else {
				return fibo(monat-1) + fibo(monat-2);
			}
		}
		else {
			return 0;
		}
	}

}
