package keyStore;

import java.util.ArrayList;

public class KeyStore02 {

	private ArrayList <String> keyStore02;

	public KeyStore02() {
		this.keyStore02 = new ArrayList <String>();
	}
		
//--------------------------------------------------------
	public boolean add (String eintrag) {
		return keyStore02.add(eintrag);
	}
//--------------------------------------------------------
	public int indexOf (String eintrag) {
		return keyStore02.indexOf(eintrag);
	}
//--------------------------------------------------------
	public void remove (int index) {
		keyStore02.remove(index);
	}
//--------------------------------------------------------
	public void remove (String eintrag) {
		keyStore02.remove(eintrag);
	}
//--------------------------------------------------------
	@Override
	public String toString() {
		return keyStore02.toString();
	}
//--------------------------------------------------------
}
