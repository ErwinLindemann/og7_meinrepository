package keyStore;

public class KeyStore01 {

	private String[] keyStore;

	public KeyStore01() {
		this.keyStore = new String[100];
	}
	
	public KeyStore01(int laenge) {
		this.keyStore = new String[laenge];
	}
	
//--------------------------------------------------------
	public boolean add (String eintrag) {
		for (int i = 0; i < this.keyStore.length; i++) {
			if (this.keyStore[i] == null) {
				this.keyStore[i]=eintrag;
				return true;
			}
		}
		return false;
	}
//--------------------------------------------------------
	public int indexOf (String eintrag) {
		for (int i = 0; i < this.keyStore.length; i++) {
			if (keyStore[i]!= null && this.keyStore[i].equals(eintrag)) {
				return i;
			}
		}
		return -1;
	}
//--------------------------------------------------------
	public void remove (int index) {
		this.keyStore[index] = null;
		for (int i = index; i < this.keyStore.length-1; i++) {
			this.keyStore[index] = this.keyStore[index+1];
		}
		this.keyStore[this.keyStore.length-1] = null;
	}
//--------------------------------------------------------
	public void remove (String eintrag) {
		for (int i = 0; i < this.keyStore.length; i++) {
			if (this.keyStore[i] != null) {
				if (this.keyStore[i].equals(eintrag)) {
					this.keyStore[i] = null;
				}
			}
		}
	}
//--------------------------------------------------------
	@Override
	public String toString() {
		String ausgabe = "";
		for (int i = 0; i < this.keyStore.length; i++) {
			if (this.keyStore[i] != null) {
				ausgabe = ausgabe + this.keyStore[i] + "\n";
			}
		}
		return ausgabe;
	}
//--------------------------------------------------------
}
