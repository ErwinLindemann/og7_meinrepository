package bubbleSort;

import java.util.Scanner;
import java.util.Random;

public class BubbleSort {

	public static void main(String[] args) {
		Scanner danny = new Scanner(System.in);
		Random rand = new Random();

		System.out.print("Gr��e des Feldes: ");
		int groesse = danny.nextInt();
		int[] feld = new int[groesse];
		System.out.println();

		for (int i = 0; i < feld.length; i++) {
			feld[i] = rand.nextInt(groesse); // 2147483647
		}

		System.out.print("|");
		for (int u = 0; u < feld.length; u++) {
			System.out.print(feld[u] + "|");
		}
		System.out.println("\n");

		System.out.println("F�r detaillierte Ausgabe 'd' - f�r normale Ausgabe 'n'");
		char x = danny.next().charAt(0);
		System.out.print("\n");
		switch (x) {
		case 'd':
			bubbleSorterMitAusgabe(feld);
			break;
		case 'n':
			bubbleSorter(feld);
			System.out.print("|");
			for (int j = 0; j < feld.length; j++) {
				System.out.print(feld[j] + "|");
			}
		default:
			break;
		}

		danny.close();
	}

	public static int[] bubbleSorter(int[] feld) {
		int tmp;
		for (int i = 1; i < feld.length; i++) {
			for (int u = 0; u < feld.length - i; u++) {
				if (feld[u] > feld[u + 1]) {
					tmp = feld[u];
					feld[u] = feld[u + 1];
					feld[u + 1] = tmp;
				}
			}
		}
		return feld;
	}

	public static int[] bubbleSorterMitAusgabe(int[] feld) {
		int temp;
		for (int i = 1; i < feld.length; i++) {
			for (int u = 0; u < feld.length - i; u++) {
				System.out.print("|");
				for (int j = 0; j < feld.length; j++) {
					System.out.print(feld[j] + "|");
				}
				System.out.println("\n|L:" + feld[u] + "-R:" + feld[u + 1] + "|");
				if (feld[u] > feld[u + 1]) {
					temp = feld[u];
					feld[u] = feld[u + 1];
					feld[u + 1] = temp;
				}
				System.out.println("|L:" + feld[u] + "-R:" + feld[u + 1] + "|");
				System.out.print("|");
				for (int j = 0; j < feld.length; j++) {
					System.out.print(feld[j] + "|");
				}
				System.out.println("\n");
			}
		}
		return feld;
	}

}