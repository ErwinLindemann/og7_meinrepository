package quicksort;

import java.util.Scanner;
import java.util.Random;

public class Quicksort {

	public static void main(String[] args) {
		Scanner danny = new Scanner(System.in);
		Random rand = new Random();

		System.out.print("Gr��e des Feldes: ");
		int groesse = danny.nextInt();
		long[] feld = new long[groesse];
		int links;
		int rechts;

		System.out.println();

		for (int i = 0; i < feld.length; i++) {
			feld[i] = rand.nextInt(groesse); // 2147483647
		}

		links = (int) feld[1];
		rechts = (int) feld[feld.length - 1];

		System.out.print("|");
		for (int u = 0; u < feld.length; u++) {
			System.out.print(feld[u] + "|");
		}
		System.out.println("\n");

		quicksort(feld, links, rechts);

		System.out.print("|");
		for (int u = 0; u < feld.length; u++) {
			System.out.print(feld[u] + "|");
		}
		System.out.println();

		danny.close();
	}

	public static void quickSorter(long feld[], int links, int rechts) {
		int tmpSort;
		if (links < rechts) {
			tmpSort = partitionL(feld, links, rechts);
			quickSorter(feld, links, tmpSort);
			quickSorter(feld, tmpSort + 1, rechts);
		}
	}

	public static int partitionL(long[] feld, int links, int rechts) {
		long pivot = feld[(links + rechts) / 2];
		int l = links - 1;
		int r = rechts + 1;

		while (l < r) {
			do {
				l++;
			} while (feld[l] < pivot);
			do {
				r--;
			} while (feld[r] > pivot);
			if (l < r) {
				long tmpPart = feld[l];
				feld[l] = feld[r];
				feld[r] = tmpPart;
			}
		}
		return r;
	}
//------------------------------------------------------------------------------------------
	public static void quicksort(long[] zahlen, int links, int rechts) {
		if (links < rechts) {
			int pivot = partition(zahlen, links, rechts);
			quicksort(zahlen, links, pivot);
			quicksort(zahlen, pivot + 1, rechts);
		}
	}

	public static int partition(long[] zahlen, int links, int rechts) {
		long pivot = zahlen[(links + rechts) / 2];
		int l = links-1;
		int r = rechts+1;
		while (l<r) {
			do{
				l++;
			}while (zahlen[l] < pivot);
			do{
				r--;
			}while (zahlen[r] > pivot);
				
			if (l < r) {
				long tmp = zahlen[l];
				zahlen[l] = zahlen[r];
				zahlen[r] = tmp;
			}
		}
		return r;
	}
	
}
