package omnom;

public class Haustier {
	private String name = "";
	private int hunger;
	private int muede;
	private int zufrieden;
	private int gesund;
	
	
	public Haustier(String name) {
		super();
		this.name = name;
		this.hunger=100;
		this.muede=100;
		this.zufrieden=100;
		this.gesund=100;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getHunger() {
		return hunger;
	}


	public void setHunger(int hunger) {
		if (hunger<0) {
			hunger=0;
		}
		if (hunger>100) {
			hunger=100;
		}
			this.hunger = hunger;
	}


	public int getMuede() {
		return muede;
	}


	public void setMuede(int muede) {
		if (muede<0) {
			muede=0;
		}
		if (muede>100) {
			muede=100;
		}
			this.muede = muede;
	}



	public int getZufrieden() {
		return zufrieden;
	}


	public void setZufrieden(int zufrieden) {
		if (zufrieden<0) {
			zufrieden=0;
		}
		if (zufrieden>100) {
			zufrieden=100;
		}
			this.zufrieden = zufrieden;
	}
	


	public int getGesund() {
		return gesund;
	}


	public void setGesund(int gesund) {
		this.gesund = gesund;
	}
	
	
	public void fuettern(int i) {
		int f = this.getHunger()+i;
		this.setHunger(f);
	}
	
	public void schlafen(int i) {
		int f = this.getMuede()+i;
		this.setMuede(f);
	}
	
	public void spielen(int i) {
		int f = this.getZufrieden()+i;
		this.setZufrieden(f);
	}
	
	public void heilen() {
		this.setGesund(100);
	}
}
