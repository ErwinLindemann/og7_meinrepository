public class Trainer extends Person{

	private char lizenzKlasse;
	private String bezahlung = "";



	public Trainer(String name, char lizenzKlasse, String bezahlung) {
		super(name);
		this.lizenzKlasse = lizenzKlasse;
		this.bezahlung = bezahlung;
	}

	public char getLizenzKlasse() {
		return lizenzKlasse;
	}
	
	public void setLizenzKlasse(char lizenzKlasse) {
		this.lizenzKlasse = lizenzKlasse;
	}
	
	public String getBezahlung() {
		return bezahlung;
	}
	
	public void setBezahlung(String bezahlung) {
		this.bezahlung = bezahlung;
	}


}
