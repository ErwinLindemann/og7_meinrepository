public class Spieler extends Person {

	private String telefonNR;
	private boolean jahresBeitragGezahl;
	private int trikotNR;




	public Spieler(String name, String telefonNR, boolean jahresBeitragGezahl, int trikotNR) {
		super(name);
		this.telefonNR = telefonNR;
		this.jahresBeitragGezahl = jahresBeitragGezahl;
		this.trikotNR = trikotNR;
	}



	public void setTelefonNR(String telefonNR) {
		this.telefonNR = telefonNR;
	}
	
	public String getTelefonNR() {
		return telefonNR;
	}

	public boolean getJahresBeitragGezahl() {
		return jahresBeitragGezahl;
	}
	
	public void setJahresBeitragGezahl(boolean jahresBeitragGezahl) {
		this.jahresBeitragGezahl = jahresBeitragGezahl;
	}
	
	public int getTrikotNR() {
		return trikotNR;
	}
	
	public void setTrikotNR(int trikotNR) {
		this.trikotNR = trikotNR;
	}


}
