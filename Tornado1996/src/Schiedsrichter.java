public class Schiedsrichter extends Person {

	private int anzahlSpielerPfiffe;



	public Schiedsrichter(String name, int anzahlSpielerPfiffe) {
		super(name);
		this.anzahlSpielerPfiffe = anzahlSpielerPfiffe;
	}

	public int getAnzahlSpielerPfiffe() {
		return anzahlSpielerPfiffe;
	}

	public void setAnzahlSpielerPfiffe(int anzahlSpielerPfiffe) {
		this.anzahlSpielerPfiffe = anzahlSpielerPfiffe;
	}


}
