public class Mannschaftsleiter {

	private String mannschaftsname;
	private int jahresBeitragsRabatt;

	public Mannschaftsleiter(String string, int jahresBeitragsRabatt) {
		super();
		this.mannschaftsname = string;
		this.jahresBeitragsRabatt = jahresBeitragsRabatt;
	}

	public String getMannschaftsname() {
		return mannschaftsname;
	}
	
	public void setMannschaftsname(String mannschaftsname) {
		this.mannschaftsname = mannschaftsname;
	}
	
	public int getJahresBeitragsRabatt() {
		return jahresBeitragsRabatt;
	}
	
	public void setJahresBeitragsRabatt(int jahresBeitragsRabatt) {
		this.jahresBeitragsRabatt = jahresBeitragsRabatt;
	}


}
