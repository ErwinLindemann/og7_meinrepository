package zoo;

public class Addon {
	private double preis;
	private String id = "";
	private String bezeichnug = "";
	private int maxAnzahl;
	private int anzahlBesitz;
	
	public Addon(double preis, String id, String bezeichnug, int maxAnzahl, int anzahlBesitz) {
		super();
		this.preis = 1.99;
		this.id = "m8b89zufa";
		this.bezeichnug = "Tierfutter";
		this.maxAnzahl = 25;
		this.anzahlBesitz = 0;
	}
	public double getPreis() {
		return preis;
	}
	public void setPreis(double preis) {
		this.preis = preis;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBezeichnug() {
		return bezeichnug;
	}
	public void setBezeichnug(String bezeichnug) {
		this.bezeichnug = bezeichnug;
	}
	public int getMaxAnzahl() {
		return maxAnzahl;
	}
	public void setMaxAnzahl(int maxAnzahl) {
		this.maxAnzahl = maxAnzahl;
	}
	public int getAnzahlBesitz() {
		return anzahlBesitz;
	}
	public void setAnzahlBesitz(int anzahlBesitz) {
		this.anzahlBesitz = anzahlBesitz;
	}
	
}
