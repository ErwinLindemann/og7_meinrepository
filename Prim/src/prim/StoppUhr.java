package prim;

public class StoppUhr {
	private long start;
	private long end;
	private long dauer;
	
	public StoppUhr() {
		
	}
	
	public void startM() {
		this.start = System.currentTimeMillis();

	}
	
	public void endM() {
		this.end = System.currentTimeMillis();

	}
	
	public long getDauer() {
		this.dauer = this.end-this.start;
		return dauer;
	}
}
