package prim;

import java.util.Scanner;

public class Prim {

	public static void main(String[] args) {
		Scanner danny = new Scanner(System.in);
		StoppUhr uhr = new StoppUhr();
		char eingabe;
		do {
			System.out.println("Bis wohin?");
			int z = danny.nextInt();
			System.out.println();
			System.out.println("Nur f�r eingegebene Zahl 'n' bis zur eingegebenen Zahl 'b'.");
			char a = danny.next().charAt(0);
			System.out.println();
			switch (a) {
			case 'n':
				uhr.startM();
				isPrimMZ(z); 																				// Nur die Zahl
				uhr.endM();
				System.out.println("Es dauert " + uhr.getDauer() + " Millisekunden.");
				break;
			case 'b':
				uhr.startM();
				isPrimM(z); 																				// Bis zur Zahl
				uhr.endM();
				System.out.println("Es dauert " + uhr.getDauer() + " Millisekunden.");
				break;
			default:
				System.out.println("Falsche Eingabe");
				break;
			}
			System.out.println();
			System.out.println("'j' zum wiederholen des Programmes.");
			eingabe = danny.next().charAt(0);
		} while (eingabe == 'j');
		danny.close();
	}

	
	public static void isPrimM(long zahl) { 																// Bis zur Zahl
		zahl = Math.abs(zahl);
		String isPrim = "Primahl";
		if (zahl < 2) {
			isPrim = "keine Primzahl";
			System.out.println(zahl + " = " + isPrim);
		} else {
			for (int i = 2; i <= zahl; i++) {
				isPrim = "Primahl";
				for (int u = 2; u <= i - 1; u++) {
					if (i % u == 0) {
						isPrim = "keine Primzahl";
					}
				}
				System.out.println(i + "\t=\t" + isPrim);
			}
		}
	}

	
	public static void isPrimMZ(long zahl) { 																// Nur die Zahl
		zahl = Math.abs(zahl);
		String isPrim = "Primahl";
		if (zahl < 2) {
			isPrim = "keine Primzahl";
			System.out.println(zahl + " = " + isPrim);
		} else {
			for (int u = 2; u <= zahl - 1; u++) {
				if (zahl % u == 0) {
					isPrim = "keine Primzahl";
				}
			}
			System.out.println(zahl + "\t=\t" + isPrim);
		}
	}
}
